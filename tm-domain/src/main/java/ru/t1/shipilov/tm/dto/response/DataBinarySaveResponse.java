package ru.t1.shipilov.tm.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public final class DataBinarySaveResponse extends AbstractResponse {
}
