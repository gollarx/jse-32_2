package ru.t1.shipilov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.shipilov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.shipilov.tm.command.AbstractCommand;

public abstract class AbstractDataCommand extends AbstractCommand {

    public AbstractDataCommand() {
    }

    @NotNull
    public IDomainEndpoint getDomainEndpoint() {
        return getServiceLocator().getDomainEndpoint();
    }

}
