package ru.t1.shipilov.tm.api.repository;

import ru.t1.shipilov.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {

}
